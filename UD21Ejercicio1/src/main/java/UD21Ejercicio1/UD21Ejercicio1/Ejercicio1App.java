package UD21Ejercicio1.UD21Ejercicio1;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import methods.MethodsCalc;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

public class Ejercicio1App extends JFrame {

	private JPanel contentPane;
	public JButton num9;
	public JButton num8;
	public JButton num7;
	public JButton num6;
	public JButton num5;
	public JButton num4;
	public JButton num3;
	public JButton num2;
	public JButton num1;
	public JButton num0;
	public static JLabel result;
	public static JLabel resultAux;
	public static JTextArea textAreaHistorial;
	public static double calc1;
	public static double calc2;
	public static String operation;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio1App frame = new Ejercicio1App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio1App() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 827, 568);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//Cada boton de la calculadora con su listener
		num9 = new JButton("9");
		num9.setBounds(239, 211, 89, 23);
		contentPane.add(num9);
		num9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "9");
			}
		});
		
		num8 = new JButton("8");
		num8.setBounds(140, 211, 89, 23);
		contentPane.add(num8);
		num8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "8");
			}
		});
		
		num7 = new JButton("7");
		num7.setBounds(41, 211, 89, 23);
		contentPane.add(num7);
		num7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "7");
			}
		});
		
		num6 = new JButton("6");
		num6.setBounds(41, 245, 89, 23);
		contentPane.add(num6);
		num6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "6");
			}
		});
		
		num5 = new JButton("5");
		num5.setBounds(141, 245, 89, 23);
		contentPane.add(num5);
		num5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "5");
			}
		});
		
		num4 = new JButton("4");
		num4.setBounds(239, 244, 89, 23);
		contentPane.add(num4);
		num4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "4");
			}
		});
		
		num3 = new JButton("3");
		num3.setBounds(41, 280, 89, 23);
		contentPane.add(num3);
		num3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "3");
			}
		});
		
		num2 = new JButton("2");
		num2.setBounds(141, 280, 89, 23);
		contentPane.add(num2);
		num2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "2");
			}
		});
		
		num1 = new JButton("1");
		num1.setBounds(238, 280, 89, 23);
		contentPane.add(num1);
		num1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "1");
			}
		});
		
		num0 = new JButton("0");
		num0.setBounds(142, 312, 89, 23);
		contentPane.add(num0);
		num0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + "0");
			}
		});
		
		JButton btnComma = new JButton(",");
		btnComma.setBounds(238, 312, 89, 23);
		contentPane.add(btnComma);
		btnComma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText() + ".");
			}
		});
		
		final JButton btnEquals = new JButton("=");
		btnEquals.setBounds(335, 312, 89, 23);
		contentPane.add(btnEquals);
		btnEquals.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc2 = Double.parseDouble(result.getText());
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		final JButton btnPlus = new JButton("+");
		btnPlus.setBounds(335, 280, 89, 23);
		contentPane.add(btnPlus);
		btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "+";
				resultAux.setText(result.getText() + "+");
				result.setText("");
			}
		});
		
		JButton btnLess = new JButton("-");
		btnLess.setBounds(335, 245, 89, 23);
		contentPane.add(btnLess);
		btnLess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "-";
				resultAux.setText(result.getText() + "-");
				result.setText("");
			}
		});
		
		JButton btnTimes = new JButton("x");
		btnTimes.setBounds(335, 211, 89, 23);
		contentPane.add(btnTimes);
		btnTimes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "x";
				resultAux.setText(result.getText() + "x");
				result.setText("");
			}
		});
		
		JButton btnDiv = new JButton("/");
		btnDiv.setBounds(335, 179, 89, 23);
		contentPane.add(btnDiv);
		btnDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "/";
				resultAux.setText(result.getText() + "/");
				result.setText("");
			}
		});
		
		JButton btnSqrd = new JButton("√");
		btnSqrd.setBounds(239, 179, 89, 23);
		contentPane.add(btnSqrd);
		btnSqrd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "√";
				resultAux.setText(result.getText() + "√");
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		JButton btnPower = new JButton("x²");
		btnPower.setBounds(140, 179, 89, 23);
		contentPane.add(btnPower);
		btnPower.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "x²";
				resultAux.setText(result.getText() + "x²");
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		JButton btn1x = new JButton("¹/x");
		btn1x.setBounds(41, 179, 89, 23);
		contentPane.add(btn1x);
		btn1x.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "¹/x";
				resultAux.setText(result.getText() + "¹/x");
				result.setText("");
				calc2 = 1;
				MethodsCalc.calculate(calc1, calc2, operation);
			}
		});
		
		JButton btnReturn = new JButton("Del");
		btnReturn.setBounds(335, 148, 89, 23);
		contentPane.add(btnReturn);
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText(result.getText().substring(0, result.getText().length() - 1));
			}
		});
		
		JButton btnPercent = new JButton("%");
		btnPercent.setBounds(41, 148, 89, 23);
		contentPane.add(btnPercent);
		btnPercent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc1 = Double.parseDouble(result.getText());
				operation = "%";
				resultAux.setText(result.getText() + "%");
				result.setText("");
			}
		});
		
		JButton btnCE = new JButton("CE");
		btnCE.setBounds(140, 148, 89, 23);
		contentPane.add(btnCE);
		btnCE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result.setText("");
			}
		});
		
		
		JButton btnC = new JButton("C");
		btnC.setBounds(239, 148, 89, 23);
		contentPane.add(btnC);
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultAux.setText("");
				result.setText("");
			}
		});
		
		//Labels de resultado y text area de historial con su label titulo
		result = new JLabel("");
		result.setFont(new Font("Tahoma", Font.PLAIN, 30));
		result.setBounds(41, 76, 383, 30);
		contentPane.add(result);
		
		resultAux = new JLabel("");
		resultAux.setFont(new Font("Tahoma", Font.PLAIN, 30));
		resultAux.setBounds(41, 35, 383, 30);
		contentPane.add(resultAux);
		
		JLabel lblHistorial = new JLabel("Historial");
		lblHistorial.setBounds(528, 33, 237, 14);
		contentPane.add(lblHistorial);
		
		textAreaHistorial = new JTextArea();
		textAreaHistorial.setEditable(false);
		textAreaHistorial.setBounds(528, 57, 237, 432);
		contentPane.add(textAreaHistorial);
	}
}
