package methods;

import UD21Ejercicio1.UD21Ejercicio1.Ejercicio1App;

public class MethodsCalc {
	public static double calculate(double calc1, double calc2, String operation) {
		//Switch que segun el operador que recibe, hace el calculo respectivo con los valores recibidos. Settea los labels de resultado y actualiza el historial.
		switch (operation) {
			case "+":
				Ejercicio1App.result.setText(String.valueOf(calc1+calc2));
				Ejercicio1App.resultAux.setText(String.valueOf(calc1+calc2));
				Ejercicio1App.textAreaHistorial.setText(Ejercicio1App.textAreaHistorial.getText() + calc1 + " + " + calc2 + " = " + String.valueOf(calc1+calc2) + "\n");
				return calc1+calc2;
			case "-":
				Ejercicio1App.result.setText(String.valueOf(calc1-calc2));
				Ejercicio1App.resultAux.setText(String.valueOf(calc1-calc2));
				Ejercicio1App.textAreaHistorial.setText(Ejercicio1App.textAreaHistorial.getText() + calc1 + " - " + calc2 + " = " + String.valueOf(calc1-calc2) + "\n");
				return calc1-calc2;
			case "x":
				Ejercicio1App.result.setText(String.valueOf(calc1*calc2));
				Ejercicio1App.resultAux.setText(String.valueOf(calc1*calc2));
				Ejercicio1App.textAreaHistorial.setText(Ejercicio1App.textAreaHistorial.getText() + calc1 + " x " + calc2 + " = " + String.valueOf(calc1*calc2) + "\n");
				return calc1*calc2;
			case "/":
				Ejercicio1App.result.setText(String.valueOf(calc1/calc2));
				Ejercicio1App.resultAux.setText(String.valueOf(calc1/calc2));
				Ejercicio1App.textAreaHistorial.setText(Ejercicio1App.textAreaHistorial.getText() + calc1 + " / " + calc2 + " = " + String.valueOf(calc1/calc2) + "\n");
				return calc1/calc2;
			case "√":
				Ejercicio1App.result.setText(String.valueOf(Math.sqrt(calc1)));
				Ejercicio1App.resultAux.setText(String.valueOf(Math.sqrt(calc1)));
				Ejercicio1App.textAreaHistorial.setText(Ejercicio1App.textAreaHistorial.getText() + " √" + calc1 + " = " + String.valueOf(Math.sqrt(calc1)) + "\n");
				return Math.sqrt(calc1);
			case "x²":
				Ejercicio1App.result.setText(String.valueOf(Math.pow(calc1, 2)));
				Ejercicio1App.resultAux.setText(String.valueOf(Math.pow(calc1, 2)));
				Ejercicio1App.textAreaHistorial.setText(Ejercicio1App.textAreaHistorial.getText() + calc1 + "² = " + String.valueOf(Math.pow(calc1, 2)) + "\n");
				return Math.pow(calc1, 2);
			case "¹/x":
				Ejercicio1App.result.setText(String.valueOf(1/calc1));
				Ejercicio1App.resultAux.setText(String.valueOf(1/calc1));
				Ejercicio1App.textAreaHistorial.setText(Ejercicio1App.textAreaHistorial.getText() + "¹/" + calc1 + " = " + String.valueOf(1/calc1) + "\n");
				return 1/calc1;
			case "%":
				Ejercicio1App.result.setText(String.valueOf(calc1/100 * calc2));
				Ejercicio1App.resultAux.setText(String.valueOf(calc1/100 * calc2));
				Ejercicio1App.textAreaHistorial.setText(Ejercicio1App.textAreaHistorial.getText() + calc1 + "% de " + calc2 + " = " + String.valueOf(calc1/100 * calc2) + "\n");
				return calc1/100 * calc2;
			default:
				return 0;
		}
	}
}
