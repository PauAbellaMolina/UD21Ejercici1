package UD21Ejercicio1.UD21Ejercicio1;

import methods.MethodsCalc;
import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class Ejercicio1AppTest extends TestCase {
	public void testSuma() {
		double resultado = MethodsCalc.calculate(2, 2, "+");
		double esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void testResta() {
		double resultado = MethodsCalc.calculate(2, 2, "-");
		double esperado = 0;
		assertEquals(esperado, resultado);
	}
	
	public void testMult() {
		double resultado = MethodsCalc.calculate(2, 2, "x");
		double esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void testDiv() {
		double resultado = MethodsCalc.calculate(4, 2, "/");
		double esperado = 2;
		assertEquals(esperado, resultado);
	}
	
	public void testSqrd() {
		double resultado = MethodsCalc.calculate(16, 0, "√");
		double esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void testElev() {
		double resultado = MethodsCalc.calculate(2, 0, "x²");
		double esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void test1DivX() {
		double resultado = MethodsCalc.calculate(2, 0, "¹/x");
		double esperado = 0.5;
		assertEquals(esperado, resultado);
	}
	
	public void testPorc() {
		double resultado = MethodsCalc.calculate(10, 100, "%");
		double esperado = 10;
		assertEquals(esperado, resultado);
	}
}
